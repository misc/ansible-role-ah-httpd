import os

import testinfra.utils.ansible_runner

testinfra_ansible_runner = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE'])
testinfra_hosts = testinfra_ansible_runner.get_hosts('ansible-test-web')

web_facts = testinfra_ansible_runner.run_module('ansible-test-web', 'setup', None)['ansible_facts']
web_ip = web_facts['ansible_default_ipv4']['address']


def test_service(host):
    srv = host.service("httpd")
    assert srv.is_running
    assert srv.is_enabled

def test_socket(host):
    host.socket("tcp://80").is_listening

def test_web_localhost(host):
    host.run_expect([0], "curl http://localhost/")

